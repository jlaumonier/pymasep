# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project (tries to) adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Unreleased]

### Added

- Scientific paper (in progress)

## [0.2] - 2024-11-02

### Added

- FREE_FOR_ALL coordination method.
- Multiagent system architecture : Container, Belief.
- Game: partial observation, observation copy. 
- Interface: Merchant interface for containers, cut scene (experimental), Dialog management.

### Fixed

- Communication fixes 

### Changed

 - Updating requirements.txt up to 2024-06-27. 
 - Switch from pygame to pygame-ce
 - Documentation API

### Removed

## [0.1] - 2023-09-05

### Added

- First release
- Multiagent system architecture: Environment, Game, Actions, State, Objects/Agents, Characteristics, Intentions, Template, Initializer, Controller.
- Loop for engine and interface
- Thread/Process communication
- Lan communication for multiplayer (listen, announcement)
- Multiagent coordination methods: TURN, WAITING_ALL