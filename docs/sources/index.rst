Welcome to PyMASEP's documentation!
===================================

This documentation tries to explain all the elements of PyMASEP v0.2.0.

The documentation is a work in progress and does not cover every aspects of the library. Do not hesitate to consult tests which are good examples of PyMASEP usage.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   architecture.rst
   mas.rst
   serialization.rst
   creation.rst
   config.rst
   communication.rst
   observation_action.rst
   bibliography.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


