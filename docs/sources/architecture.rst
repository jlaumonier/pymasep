General Architecture
====================

Py MultiAgent System Engine Plateform is a kind of game engine and also a sort of multiagent simulation engine. Every Pymasep-based application is composed of an engine and optionally, one or more interfaces that are connected to the engine.

.. image:: images/pymasepexplain-18.png

Thus, Pymasep is composed of multiple modules. The `Application` module is the base of `Interface` and `Engine` modules. These modules also rely on the `Communication`, that allow information transfer between the different parts of the application. At last, all these parts manipulate concepts from the `Common` module.

The engine part runs the game and maintains all the information needed to make the game evolve according to action of all agents. The engine sends partial information to all interfaces according to the part of the game they need to display. If the interface representing a human player, it sends actions choose by this player.

More details of the multi-agent system architecture defined in the `Common` module is details in the :doc:`mas` section.

