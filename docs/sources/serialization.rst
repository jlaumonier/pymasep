.. _serialization:

Serialization
=============

Serialization is made in xml and used by Engine and Interface to communicate.
Any BaseObject must have a `name`, which defines its id. Other attributes are optionals. `template` defines how the BaseObject is constructed (mainly list of Characteristics) and `initializer` how the BaseObject will be initialized (mainly value of the characteristics). The `state` represents the creation status of the BaseObject (currently initializing or initialized and ready to run).

The format is defined as follows :

xml
---

.. code-block:: xml

    <BaseObject name="" [nature=""] [state=""] [template=""] [initializer=""] />

    <Object name="" [nature=""] [state=""] [template=""] [initializer=""] />

    <Container name="" [state=""] [template=""] [initializer=""] />

    <ObjectState name="" [state=""] [template=""] [initializer=""] />

    <Characteristic name="" [state=""] [template=""] [initializer=""]>
        <type>value</type>
    </Characteristic>

    <State name="" state="" is_final="" env_step="" [template=""] [initializer=""]/>

    <Action type="" />

    <Intention />





