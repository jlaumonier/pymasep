.. _config:

Configuration
=============

Config files are defined with yaml format. The default configuration file, `app.yaml`, is the `config` directory.

config example ::

    logger:
        level: <logging level>
    data_path: <path of data files>
    interface:
        resolution: [<x_res>, <y_res>]
    game:
        max_nb_players: <max number of players>.
    engine:
        eps: <number of engine loop per second>
        server:
            port: <port of the server to listen to, for general communication>

