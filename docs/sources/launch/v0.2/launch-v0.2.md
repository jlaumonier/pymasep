Depuis longtemps, j'ai toujours voulu créer des jeux. Cette envie s'est transformé en utilisant le medium video-ludique à partir du moment où je n'ai pas pu m'acheter une NES, que mon père m'a vendu à la place son 8088 et que j'ai découvert que je pouvais en créer moi même. Je vous montre ci dessous un de mes premiers jeux en DOS/QuickBasic/CGA. C'est codé avec les pieds mais ça a "fonctionné" jusqu'a ce que je me heurte à des problèmes techniques diificilement surmontable pour mon age et la super technologie choisie.

![s&s en Basic](sets-basic.gif)

J'ai repris récemment ce genre de création dans des technologie un peu plus modernes, durables et un développement très artisanal sans aucune prétention à une quelconque utilité du résultat. J'ai décidé, pour commencer, de coder les fameux jeux en encart de Jeux et Strategie dont les plus agés pouront se rappeler. Je commence par le Sceptre Maudit, jeux en encart du J&S no 38, car c'est le premier auquel j'ai joué. Je sors aujourd'hui la version 0.2. C'est codé en python et c'est basé sur une bibliothèque, pymasep, semi moteur de jeux/simulation multiagent, que je suis également en train de concevoir. En complément, j'utilise cette bibliothèque pour créer un autre jeu vidéo de mon cru basé sur mes experiences de MJ sur différents jeux de rôle.

Parmi les fonctionnalités de la version 0.2 du Sceptre maudit:
- Jeu multi-joueurs sur reseau local.
- Création du personnage avec quelques caractéristiques.
- Achat des armes.
- Déplacement sur la carte.
- Ouverture (destruction) des portes.

![Sceptre maudit v0.2](sceptremaudit-v0.2.gif)

Le code source est disponible sur https://gitlab.com/jlaumonier/encartgames.git sous licence CC-BY-SA-NC 4.0. La raison du NC est que les graphismes ne m'appartiennent pas mais il appartiennent à l'auteur original, Gildas Sagot (https://fr.wikipedia.org/wiki/Gildas_Sagot), pour les graphiques. Merci à lui d'ailleurs.

La version 0.2 de pymasep est également disponible avec une licence MIT. Elle est disponible sur PyPI et sur https://gitlab.com/jlaumonier/pymasep

Voilà. Tout commentaire est le bienvenu.
