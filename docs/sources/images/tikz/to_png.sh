#!/bin/bash

xelatex -synctex=1 -interaction=nonstopmode "pymasepexplain".tex
convert -density 300 pymasepexplain.pdf ../pymasepexplain.png