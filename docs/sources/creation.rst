Creation
========

The state creation process can be done by code, by creating the :py:class:`pymas.state`, all Objects with :meth:`environment.create_object` and adding them to the state with :py:meth:`State.add_object()`.

.. _templates:

Templates
---------

A template allows to define the structure of the child of an object. It is mainly used to define characteristics of a Object/ObjectState

In the `Game` class, the following templates are defined :
 * EmptyIntentionTemplate : create an `Intention` class with no action associated
 * DefaultStateTemplate : create a `State` class with no object.
 * SystemObjectTemplate : create a special object containing system characteristics (AgentOrder)
 * SystemObjectStateTemplate : create a special ObjectState containing system characteristics (AgentOrder)
 * AgentOrder : create a system characteristic containing the agent order as a `CircularHeadList`
 * SystemStateTemplate : create a `State` containing the system object defined by SystemObjectTemplate

A template is created using a json structure (see `Template`  class).

More templates can be defined in each application using the template.json file

.. _initializers:

Initializers
------------

Once the object/agent structure is created, initializers are used to set the values of the existing characteristics of the objects. Once created, the initializer can be applied to the corresponding BaseObject using `apply()` method.

Initializer are defined as a hierarchy according to the BaseObject to initialize and if the initializer is automatic (in engine) or need user input (interface).

Initializer

  InitializerCharacInterface

    InitializerCharacInterfaceShared

  InitializerContainer

    InitializerContainerInterface

  InitializerFixed

  InitializerObject

    InitializerObjectInterface

  InitializerObjectState

     InitializerObjectStateInterface

As template, initializers are defined using a json structure. See `Initializer` and each subclass for details.

Initialization from xml
-----------------------

In general, to initialize a BaseObject, either the initializer or a sub element must be present in the xml. The state attribute must not be present since it is calculated by the BaseObject according to the template/initializer/sub element presence. The state value 'ìnit' and 'run' is used automatically only by the interaction engine/interface.

Some rules apply to create a structure of BaseObject from xml.
 * `State`
    * `template` : if a State template is given, all objects are created in the state
    * After all objects are created from the template, all objects in the xml are created in the state.
 * `Characteristic`
    * The initialization depends on the state of the Characteristic:
        * creation : template and/or sub element must be present.
        * init : the characteristic is created but need an input from the user. if `initializer` is present, it must be a Interface initializer. if a sub element is present, the value must be None (e.g. <int />)
        * run : the characteristic is initialized. `template` and `initializer` must not be present, only a sub element.
 * ObjectState
    * The initialization depends on the state of the ObjectState (and its characteristics):
        * creation : all characteristics in 'template' and sub element are created (first characteristic from template and then characteristic from sub element). If the 'template' is present, then the `initializer` must also be present to initialize all characteristics. Of course, all characteristics in template union sub element must have an initializer to initialize them.
        * init : all characteristics are created from sub element and initialized either by sub element or by initializer
        * run : all characteristics are create from sub element. No `template` or `initializer` are taking into account.
 * Object
     * The initialization depends on the state of the Object (and its ObjectState/Characteristics):
        * creation : ObjectState is created from `template` and sub element (first from template and then from sub element). Characteristics created from template AND sub element are merged. if `initializer` is present, it is applied. if no sub element is present template and initializer are needed. Of course, all characteristics in template union sub element must have an initializer to initialize them.
        * init/run : the ObjectState is initialized from the sub element.
 * Agent:
    * The initialization is the same of the Object but it takes in run state, an Intention BaseObject.
 * Intention
    * You should not create an intention from xml. An intention should be created by code usually in a `Controller` as described below.

    .. code-block:: python

        self.next_action = self.environment.create_action(Game.ACTION_INTENTION)
        action_intention = self.environment.create_action(Game.ACTION_OPEN)
        action_intention.params[<name of parameter>] = <parameter_value>
        self.next_action.params['action'] = action_intention

 * Container
    * The initialization depends on the state of the Container.
       * creation : Container may contains some sub elements (Objects) or not. The Container is always created at 'init' state
       * init : The Initializer must be present a must be an InterfaceInitializer.
       * run : the container is initialized. `template` and `initializer` must not be present, only sub elements. This state is set by the initializer of the container.


