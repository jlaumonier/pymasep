# content of conftest.py
import pytest

from test.unit.mocks.game_mock import GameMock

pytest_plugins = ["pymasep.tests.fixtures"]

@pytest.fixture(scope="function")
def game_class():
    """
    Create a game according to the current project
    """
    yield GameMock




