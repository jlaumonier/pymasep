from omegaconf import OmegaConf

import pymasep as pm

from test.unit.mocks.game_mock import GameMock
from test.unit.mocks.app_mock import AppMock


def test_constructor_interface_local():
    app = AppMock()
    cfg = OmegaConf.create()
    cfg['root_path'] = '.'
    interface = pm.interface.Interface(app=app,
                                       received_q_id=0,
                                       cfg=cfg,
                                       game_class_=GameMock,
                                       interface_role=pm.application.ConnectedSubAppInfo.ROLE_NONE)
    assert isinstance(interface.environment, pm.common.Environment)
    assert interface.remote_communication is None


def test_constructor_interface_remote_known():
    app = AppMock()
    cfg = OmegaConf.create()
    cfg['root_path'] = '.'
    try:
        _ = pm.interface.Interface(app=app, received_q_id=0,
                                   cfg=cfg,
                                   game_class_=GameMock,
                                   remote_engine_host='localhost',
                                   remote_engine_port=55001,
                                   interface_role=pm.application.ConnectedSubAppInfo.ROLE_NONE)
    except ConnectionRefusedError:
        # error b
        assert True
    else:
        assert False


def test_constructor_interface_remote_unknown():
    app = AppMock()
    cfg = OmegaConf.create()
    cfg['root_path'] = '.'
    try:
        _ = pm.interface.Interface(app=app, received_q_id=0,
                                   cfg=cfg,
                                   game_class_=GameMock,
                                   interface_role=pm.application.ConnectedSubAppInfo.ROLE_NONE,
                                   search_remote=True)
    except ConnectionError:  # No server answered
        assert True
    else:
        assert False
