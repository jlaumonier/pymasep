from pymasep.dices import roll_dice, roll_dices, min_max_dice, min_max_dices


def test_roll_dice_one_dice():
    dice = '1D6'

    total, values = roll_dice(dice)

    assert total in [1, 2, 3, 4, 5, 6]
    assert values in [[1], [2], [3], [4], [5], [6]]


def test_roll_dice_two_dices():
    dice = '2D4'

    total, values = roll_dice(dice)

    assert total in [2, 3, 4, 5, 6, 7, 8]
    assert values[0] in [1, 2, 3, 4]
    assert values[1] in [1, 2, 3, 4]


def test_roll_dice_not_a_dice():
    dice = '2e4'

    total, values = roll_dice(dice)

    assert total == 0
    assert values == []


def test_roll_dice_not_a_dice_with_letter():
    dice = '2D4e'

    total, values = roll_dice(dice)

    assert total == 0
    assert values == []


def test_roll_dice_not_a_dice_with_letter_begin():
    dice = 'eD4'

    total, values = roll_dice(dice)

    assert total == 0
    assert values == []


def test_roll_dices_one_dice():
    dices = '1D6'

    total, values = roll_dices(dices)

    assert total in [1, 2, 3, 4, 5, 6]
    assert values in [[1], [2], [3], [4], [5], [6]]


def test_roll_dices_one_dice_plus_number():
    dices = '1D6+1'

    total, values = roll_dices(dices)

    assert total in [2, 3, 4, 5, 6, 7]
    assert values in [[1, 1], [2, 1], [3, 1], [4, 1], [5, 1], [6, 1]]


def test_roll_dices_one_dice_minus_number():
    dices = '1D6-1'

    total, values = roll_dices(dices)

    assert total in list(range(0, 6))
    assert values[0] in list(range(1, 7))
    assert values[1] in [-1]


def test_roll_dices_complex_expression():
    dices = '1D6+1-1D4+2'

    total, values = roll_dices(dices)

    assert total in list(range(0, 9))
    assert values[0] in list(range(1, 7))
    assert values[1] in [1]
    assert values[2] in list(range(-4, 0))
    assert values[3] in [2]


def test_roll_dices_statistical_test():
    for i in range(1000):
        dices = '1D20-1-3D4+2'

        total, values = roll_dices(dices)

        assert total in list(range(-10, 19))
        assert values[0] in list(range(1, 21))
        assert values[1] in [-1]
        assert values[2] in list(range(-4, 0))
        assert values[3] in list(range(-4, 0))
        assert values[4] in list(range(-4, 0))
        assert values[5] in [2]


def test_min_max_dice():
    dice = '1D6'

    min, max = min_max_dice(dice)

    assert min == 1
    assert max == 6

def test_min_max_three_dices():
    dice = '3D6'

    min, max = min_max_dice(dice)

    assert min == 3
    assert max == 18

def test_min_max_error():
    dice = 'eD6'

    min, max = min_max_dice(dice)

    assert min == 0
    assert max == 0

def test_min_max_dices_complex_expression():
    dices = '2D6+1D4+3'

    min, max = min_max_dices(dices)

    assert min == 6
    assert max == 19

def test_min_max_dices_complex_expression_2():
    dices = '1D20-1-3D4+2'

    min, max = min_max_dices(dices)

    assert min == -10
    assert max == 18