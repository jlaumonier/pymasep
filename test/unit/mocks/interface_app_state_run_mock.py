from pymasep.interface.interface_state import InterfaceState


class InterfaceStateRunMock(InterfaceState):

    def __init__(self, sub_application):
        super().__init__(sub_application)

    def init(self):
        pass

    def handle_event(self, event):
        pass

    def render(self, time_delta):
        self.sub_application.logger.info('state run')
