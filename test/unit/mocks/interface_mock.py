import pymasep as pm
from test.unit.mocks.game_mock import GameMock
from test.unit.mocks.controller_mock import ControllerMock
from test.unit.mocks.interface_app_state_init_mock import InterfaceStateInitMock
from test.unit.mocks.interface_app_state_run_mock import InterfaceStateRunMock


class InterfaceMock(pm.interface.Interface):

    def __init__(self, app, received_q_id, cfg, remote_engine_host=None, remote_engine_port=None, search_remote=False):
        super().__init__(app, received_q_id, cfg, GameMock, pm.application.ConnectedSubAppInfo.ROLE_ACTOR,
                         remote_engine_host=remote_engine_host,
                         remote_engine_port=remote_engine_port,
                         search_remote=search_remote)
        self.controller_interface_class_ = ControllerMock

        self.app_states[InterfaceStateInitMock.__name__] = InterfaceStateInitMock(self)
        self.app_states[InterfaceStateRunMock.__name__] = InterfaceStateRunMock(self)
        self.push_state(self.app_states[InterfaceStateInitMock.__name__])
