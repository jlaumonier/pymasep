from pymasep.common import Controller, State, Agent
from test.unit.mocks.game_mock import GameMock


class ControllerWaitBeforeActMock(Controller):
    """
    Action sent by this controller, alternatively
    - None
    - ACTION_TYPE_2
    """

    def __init__(self, environment):
        super().__init__(environment)
        self.max_wait = 2
        self.wait = 0

    def action_choice(self, observation: State, agent: Agent):
        """
        Choose the action. Wait 2 calls before choosing Action.ACTION_TYPE_2
        :param observation the observation used to choose the action
        :param agent the agent who choose the action
        :return the action chosen for the agent.
        """
        self.wait += 1
        result = None
        if self.wait >= self.max_wait:
            result = self.environment.create_action(GameMock.ACTION_TYPE_2)
            self.wait = 0
        return result
