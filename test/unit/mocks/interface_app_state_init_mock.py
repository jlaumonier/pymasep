from pymasep.interface.interface_state import InterfaceState
from test.unit.mocks.interface_app_state_run_mock import InterfaceStateRunMock


class InterfaceStateInitMock(InterfaceState):

    def __init__(self, sub_application):
        super().__init__(sub_application)

    def init(self):
        pass

    def handle_event(self, event):
        pass

    def update(self):
        super().update()
        self.sub_application.push_state(self.sub_application.app_states[InterfaceStateRunMock.__name__])

    def render(self, time_delta):
        self.sub_application.logger.info('state init')
