from pymasep.common import Controller, State, Agent


class NoneActionControllerMock(Controller):

    def action_choice(self, observation: State, agent: Agent):
        return None
