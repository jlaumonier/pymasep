from pymasep.common import Controller, State, Agent
from test.unit.mocks.game_mock import GameMock


class ControllerTwoActionsMock(Controller):
    """
    Action sent by this controller, alternatively
    - ACTION_TYPE_1
    - ACTION_TYPE_2
    """

    def __init__(self, environment):
        super().__init__(environment)
        self.wait = 0

    def action_choice(self, observation: State, agent: Agent):
        """
        Choose the action. Choose alternatively Action.ACTION_TYPE_1 and Action.ACTION_TYPE_2
        :param observation the observation used to choose the action
        :param agent the agent who choose the action
        :return the action chosen for the agent.
        """
        if self.wait == 0:
            result = self.environment.create_action(GameMock.ACTION_TYPE_1)
            self.wait += 1
        else:
            result = self.environment.create_action(GameMock.ACTION_TYPE_2)
            self.wait = 0
        return result
