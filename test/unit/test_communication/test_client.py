from pymasep.communication import Client, SocketHandler


def test_init():
    client = Client(ident="client",
                    host='localhost',
                    port=50001,
                    socket_handler_class=SocketHandler,
                    socket_handler_args=tuple())

    assert client.id == 'client'
    assert client.host == 'localhost'
    assert client.port == 50001


def test_init_no_host_nor_port():
    client = Client(ident="client",
                    socket_handler_class=SocketHandler,
                    socket_handler_args=tuple())

    assert client.id == 'client'
    assert client.host is None
    assert client.port is None
