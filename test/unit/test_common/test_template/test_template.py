from pymasep.common.template import Template


def test_init_characteristic_template_should_init(mock_game):
    template_dict = {"name": "nameCharac",
                     "created_class": "Characteristic",
                     "value_type": "int"}

    template = Template(mock_game, template_dict)

    assert template.name == 'nameCharac'
    assert template.nature is None
    assert template.created_class == 'Characteristic'
    assert template.value_type == int
    assert template.game == mock_game


def test_init_object_state_template_should_init(mock_game):
    template_dict = {"name": "DefaultObjectStateTemplate",
                     "nature": "objectstate",
                     "created_class": "ObjectState",
                     "subclass_templates": ["Charac1", "Charac2"]}
    template_c1_dict = {"name": "Charac1", "created_class": "Characteristic", "value_type": "int"}
    template_c2_dict = {"name": "Charac2", "created_class": "Characteristic", "value_type": "int"}
    template_c1 = Template(mock_game, template_c1_dict)
    template_c2 = Template(mock_game, template_c2_dict)
    mock_game.add_template(template_c1)
    mock_game.add_template(template_c2)

    template = Template(mock_game, template_dict)

    assert template.name == 'DefaultObjectStateTemplate'
    assert template.nature == 'objectstate'
    assert template.created_class == 'ObjectState'
    assert template_c1 in template.subclass_templates
    assert template_c2 in template.subclass_templates
    assert template.game == mock_game


def test_init_with_subclass_inheritance(mock_game):
    template_c1_dict = {"name": "Charac1", "created_class": "Characteristic", "value_type": "int"}
    template_c2_dict = {"name": "Charac2", "created_class": "Characteristic", "value_type": "int"}
    template_dict1 = {"name": "ObjectStateTemplate1",
                      "nature": "objectstate",
                      "created_class": "ObjectState",
                      "subclass_templates": ["Charac1"]}
    template_dict2 = {"name": "ObjectStateTemplate2",
                      "nature": "objectstate",
                      "created_class": "ObjectState",
                      "subclass_inheritance": ["ObjectStateTemplate1"],
                      "subclass_templates": ["Charac2"]}
    template_c1 = Template(mock_game, template_c1_dict)
    template_c2 = Template(mock_game, template_c2_dict)
    mock_game.add_template(template_c1)
    mock_game.add_template(template_c2)
    template1 = Template(mock_game, template_dict1)
    mock_game.add_template(template1)

    template2 = Template(mock_game, template_dict2)

    assert template_c1 in template2.subclass_templates
    assert template_c2 in template2.subclass_templates
