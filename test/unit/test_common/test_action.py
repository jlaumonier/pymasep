from xml.etree.ElementTree import tostring, fromstring

import pytest

import pymasep.common

from test.unit.mocks.game_mock import GameMock


def test_init(mock_environment):
    ac = pymasep.common.Action(environment=mock_environment, action_type=GameMock.ACTION_TYPE_1)
    assert ac.environment == mock_environment
    assert ac.type == GameMock.ACTION_TYPE_1
    assert ac.params == dict(), ac.params


def test_to_xml_transformed(mock_environment):
    expected_xml_str = '<Action type="1">' \
                       '<ActionParameter name="OneParam">' \
                       '<Characteristic name="Charac1" state="run"><int>1</int></Characteristic>' \
                       '</ActionParameter>' \
                       '<ActionParameter name="ParamTwo">' \
                       '<tuple><item><int>1</int></item><item><int>2</int></item></tuple>' \
                       '</ActionParameter>' \
                       '' \
                       '</Action>'
    action = pymasep.common.Action(mock_environment, GameMock.ACTION_TYPE_1)
    charac = mock_environment.create_object(name='Charac1', template=mock_environment.game.templates['CharacInt'])
    charac.value = 1
    action.add_parameter('OneParam', charac)
    t = (1, 2)
    action.add_parameter('ParamTwo', t)

    xml_str_result = tostring(action.to_xml(), encoding='unicode')

    assert xml_str_result == expected_xml_str


def test_from_xml(mock_environment):
    action_xml_str = '<Action type="1">' \
                     '<ActionParameter name="OneParam">' \
                     '<Characteristic name="CharacInt" state="run"><int>1</int></Characteristic>' \
                     '</ActionParameter>' \
                     '<ActionParameter name="ParamTwo">' \
                     '<tuple><item><int>1</int></item><item><int>2</int></item></tuple>' \
                     '</ActionParameter>' \
                     '</Action>'
    xml_node = fromstring(action_xml_str)
    action = mock_environment.create_action(GameMock.ACTION_NO_ACTION)

    action.from_xml(environment=mock_environment, xml_node=xml_node)

    assert isinstance(action, pymasep.common.Action)
    assert action.type == 1
    assert len(action.params) == 2
    assert isinstance(action.params['OneParam'], pymasep.common.Characteristic)
    assert action.params['ParamTwo'] == (1, 2)


def test_from_xml_none_parameter(mock_environment):
    action_xml_str = '<Action type="1">' \
                     '<ActionParameter name="OneParam">' \
                     '<NoneType>None</NoneType>' \
                     '</ActionParameter>' \
                     '</Action>'
    xml_node = fromstring(action_xml_str)
    env = pymasep.common.Environment()
    game = GameMock()
    env.game = game
    action = env.create_action(GameMock.ACTION_NO_ACTION)

    action.from_xml(environment=env, xml_node=xml_node)

    assert action.params['OneParam'] is None


def test_from_xml_param_is_action(mock_environment):
    action_xml_str = '<Action type="1">' \
                     '<ActionParameter name="action">' \
                     '<Action type="5">' \
                     '<ActionParameter name="pos">' \
                     '<list>' \
                     '<item>' \
                     '<tuple><item><int>14</int></item><item><int>18</int></item></tuple>' \
                     '</item></list>' \
                     '</ActionParameter>' \
                     '</Action>' \
                     '</ActionParameter>' \
                     '</Action>'
    xml_node = fromstring(action_xml_str)
    action = mock_environment.create_action(GameMock.ACTION_NO_ACTION)

    action.from_xml(environment=mock_environment, xml_node=xml_node)

    assert isinstance(action, pymasep.common.Action)
    assert action.type == 1
    assert len(action.params) == 1
    assert isinstance(action.params['action'], pymasep.common.Action)
    assert action.params['action'].params['pos'] == [(14, 18)]


def test_id_str(mock_environment):
    action_xml_str = '<Action type="1">' \
                     '<ActionParameter name="OneParam">' \
                     '<Characteristic name="Charac1" state="run"><int>1</int></Characteristic>' \
                     '</ActionParameter>' \
                     '<ActionParameter name="ParamTwo">' \
                     '<tuple><item><int>1</int></item><item><int>2</int></item></tuple>' \
                     '</ActionParameter>' \
                     '</Action>'
    xml_node = fromstring(action_xml_str)
    action = mock_environment.create_action(GameMock.ACTION_NO_ACTION)
    action.from_xml(environment=mock_environment, xml_node=xml_node)

    id_str = action.id_str()

    assert id_str == 'action1OneParamCharac11ParamTwo(1,2)'


@pytest.mark.parametrize('action_xml_str',
                         [('<Action type="1">'
                           '<ActionParameter name="OneParam">'
                           '<Characteristic name="Charac1" state="run"><int>1</int></Characteristic>'
                           '</ActionParameter>'
                           '<ActionParameter name="ParamTwo">'
                           '<tuple><item><int>1</int></item><item><int>2</int></item></tuple>'
                           '</ActionParameter>'
                           '</Action>'),
                          ('<Action type="1"><ActionParameter name="action">'
                           '<Action type="5"><ActionParameter name="pos"><list><item>'
                           '<tuple><item><int>14</int></item><item><int>18</int></item></tuple>'
                           '</item></list></ActionParameter></Action></ActionParameter></Action>')])
def test_copy_obs(mock_environment, action_xml_str):
    xml_node = fromstring(action_xml_str)
    action = mock_environment.create_action(GameMock.ACTION_NO_ACTION)
    action.from_xml(environment=mock_environment, xml_node=xml_node)

    result = action.copy_obs(params={})

    assert result != action
    assert result.environment == action.environment
    assert len(result.params) == len(action.params)
    for param_name, param in result.params.items():
        assert param_name in action.params.keys()
        param_type = type(param)
        if param_type in [int, float, tuple]:
            assert param == action.params[param_name]
        else:
            assert param != action.params[param_name]
