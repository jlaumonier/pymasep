from pymasep.common.initializer import Initializer
from test.unit.mocks.game_mock import GameMock


def test_init_should_init():
    game = GameMock()

    initializer = Initializer(game, {'name': 'OneInitializer', 'value_type': "int"})

    assert initializer.name == 'OneInitializer'
    assert initializer.value_type == int


def test_init_with_sub_initializer_should_init():
    game = GameMock()

    game.add_initializer(Initializer(game, {'name': 'subinit1', 'value_type': "int"}))
    game.add_initializer(Initializer(game, {'name': 'subinit2', 'value_type': "str"}))
    game.add_initializer(Initializer(game, {'name': 'containerinit1', 'value_type': "str"}))
    game.add_initializer(Initializer(game, {'name': 'containerinit2', 'value_type': "str"}))

    initializer = Initializer(game, {'name': 'OneInitializer',
                                     'value_type': "dict",
                                     'subclass_initializer': {'subelement1': 'subinit1',
                                                              'subelement2': 'subinit2',
                                                              'containers': [('containername1', 'containerinit1'),
                                                                             ('containername2', 'containerinit2')]}})

    assert initializer.subclass_initializer['subelement1'] == game.initializers['subinit1']
    assert initializer.subclass_initializer['subelement2'] == game.initializers['subinit2']
    assert len(initializer.subclass_initializer['containers']) == 2
    assert initializer.subclass_initializer['containers'][0] == ('containername1', game.initializers['containerinit1'])
    assert initializer.subclass_initializer['containers'][1] == ('containername2', game.initializers['containerinit2'])
