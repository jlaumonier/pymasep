from xml.etree.ElementTree import fromstring

from pymasep.common.initializer import InitializerCharacInterface, InitializerCharacInterfaceShared


def test_filter_choice_no_other(mock_environment):
    possible_values = [('1', 'A'),
                       ('2', 'B'),
                       ('3', 'C')]

    state_xml_str = '<State name="State" env_step="1">' \
                    '<Object name="object1">' \
                    '<ObjectState name="state">' \
                    '<Characteristic name="Step0" initializer="SharedInit"><str /></Characteristic>' \
                    '</ObjectState>' \
                    '</Object>' \
                    '</State>'
    xml_node = fromstring(state_xml_str)
    initializer = InitializerCharacInterfaceShared(mock_environment.game,
                                                   {'name': 'SharedInit',
                                                    'value_type': "str",
                                                    'value_mode': InitializerCharacInterface.VALUE_MODE_CHOICE,
                                                    'param': possible_values})
    mock_environment.game.add_initializer(initializer)
    state = mock_environment.create_base_object(name='', xml_node=xml_node)

    result = initializer.filter_choice(charac_name='Step0', state=state)

    assert result == possible_values


def test_filter_choice_one_set(mock_environment):
    possible_values = [('1', 'A'),
                       ('2', 'B'),
                       ('3', 'C')]

    state_xml_str = '<State name="State" env_step="1">' \
                    '<Object name="object1">' \
                    '<ObjectState name="state">' \
                    '<Characteristic name="Step0" initializer="SharedInit"><str>A</str></Characteristic>' \
                    '</ObjectState>' \
                    '</Object>' \
                    '</State>'
    xml_node = fromstring(state_xml_str)
    initializer = InitializerCharacInterfaceShared(mock_environment.game,
                                                   {'name': 'SharedInit',
                                                    'value_type': "str",
                                                    'value_mode': InitializerCharacInterface.VALUE_MODE_CHOICE,
                                                    'param': possible_values})
    mock_environment.game.add_initializer(initializer)
    state = mock_environment.create_object(name='', xml_node=xml_node)

    result = initializer.filter_choice(charac_name='Step0', state=state)

    assert len(initializer.param) == 3
    assert len(possible_values) == 3
    assert result == [('2', 'B'), ('3', 'C')]


def test_filter_choice_two_set(mock_environment):
    possible_values = [('1', 'A'),
                       ('2', 'B'),
                       ('3', 'C')]

    state_xml_str = '<State name="State" env_step="1">' \
                    '<Object name="object1">' \
                    '<ObjectState name="state">' \
                    '<Characteristic name="Step0" initializer="SharedInit"><str>A</str></Characteristic>' \
                    '</ObjectState>' \
                    '</Object>' \
                    '<Object name="object2">' \
                    '<ObjectState name="state">' \
                    '<Characteristic name="Step0" initializer="SharedInit"><str>C</str></Characteristic>' \
                    '</ObjectState>' \
                    '</Object>' \
                    '</State>'
    xml_node = fromstring(state_xml_str)
    initializer = InitializerCharacInterfaceShared(mock_environment.game,
                                                   {'name': 'SharedInit',
                                                    'value_type': "str",
                                                    'value_mode': InitializerCharacInterface.VALUE_MODE_CHOICE,
                                                    'param': possible_values})
    mock_environment.game.add_initializer(initializer)
    state = mock_environment.create_object(name='', xml_node=xml_node)

    result = initializer.filter_choice(charac_name='Step0', state=state)

    assert result == [('2', 'B')]


def test_filter_choice_three_set(mock_environment):
    possible_values = [('1', 'A'),
                       ('2', 'B'),
                       ('3', 'C')]

    state_xml_str = '<State name="State" env_step="1">' \
                    '<Object name="object1">' \
                    '<ObjectState name="state">' \
                    '<Characteristic name="Step0" initializer="SharedInit"><str>A</str></Characteristic>' \
                    '</ObjectState>' \
                    '</Object>' \
                    '<Object name="object2">' \
                    '<ObjectState name="state">' \
                    '<Characteristic name="Step0" initializer="SharedInit"><str>C</str></Characteristic>' \
                    '</ObjectState>' \
                    '</Object>' \
                    '<Object name="object3">' \
                    '<ObjectState name="state">' \
                    '<Characteristic name="Step0" initializer="SharedInit"><str>B</str></Characteristic>' \
                    '</ObjectState>' \
                    '</Object>' \
                    '</State>'
    xml_node = fromstring(state_xml_str)
    initializer = InitializerCharacInterfaceShared(mock_environment.game,
                                                   {'name': 'SharedInit',
                                                    'value_type': "str",
                                                    'value_mode': InitializerCharacInterface.VALUE_MODE_CHOICE,
                                                    'value_mode': 'shared',
                                                    'param': possible_values})
    mock_environment.game.add_initializer(initializer)
    state = mock_environment.create_object(name='', xml_node=xml_node)

    result = initializer.filter_choice(charac_name='Step0', state=state)

    assert result == []


def test_filter_choice_two_set_one_dont_have_charac(mock_environment):
    possible_values = [('1', 'A'),
                       ('2', 'B'),
                       ('3', 'C')]

    state_xml_str = '<State name="State" env_step="1">' \
                    '<Object name="object1">' \
                    '<ObjectState name="state">' \
                    '<Characteristic name="Step0" initializer="SharedInit"><str>A</str></Characteristic>' \
                    '</ObjectState>' \
                    '</Object>' \
                    '<Object name="object2">' \
                    '<ObjectState name="state">' \
                    '<Characteristic name="Step1"><str>C</str></Characteristic>' \
                    '</ObjectState>' \
                    '</Object>' \
                    '</State>'
    xml_node = fromstring(state_xml_str)
    initializer = InitializerCharacInterfaceShared(mock_environment.game,
                                                   {'name': 'SharedInit',
                                                    'value_type': "str",
                                                    'value_mode': InitializerCharacInterface.VALUE_MODE_CHOICE,
                                                    'param': possible_values})
    mock_environment.game.add_initializer(initializer)
    state = mock_environment.create_object(name='', xml_node=xml_node)

    result = initializer.filter_choice(charac_name='Step0', state=state)

    assert result == [('2', 'B'), ('3', 'C')]
