from pymasep.common.initializer import InitializerObject


def test_apply(mock_environment):
    game = mock_environment.game
    initializer = InitializerObject(game, {'name': 'InitializerObject',
                                           "subclass_initializer": {
                                               'objectstate': "ObjStInitializer",
                                               'containers': [("container1", "OneInitializerContainer")]}})

    obj = mock_environment.create_object(name='object1',
                                         template=game.templates['ContainerObjectTemplate'])
    initializer.apply(obj)

    assert obj.name == 'object1'
    assert len(obj.containers) == 1
    assert obj.containers['container1'].name == 'container1'
    assert len(obj.containers['container1']) == 1
    assert obj.containers['container1']['object1'].object_state.characteristics['CharacInt'].value == 1


def test_apply_container_init_interface(mock_environment):
    game = mock_environment.game
    initializer = InitializerObject(game, {'name': 'InitializerObject',
                                           "subclass_initializer": {
                                               'objectstate': "ObjStInitializer",
                                               'containers': [("container1", "OneInitializerContainerIntf")]}})

    obj = mock_environment.create_object(name='object1',
                                         template=game.templates['ContainerObjectTemplate'])
    initializer.apply(obj)

    assert obj.name == 'object1'
    assert len(obj.containers) == 1
    assert obj.containers['container1'].name == 'container1'
    assert len(obj.containers['container1']) == 0
    assert obj.containers['container1'].state == 'init'
    assert obj.containers['container1'].initializer == game.initializers['OneInitializerContainerIntf']
