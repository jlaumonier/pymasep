from pymasep.common.initializer import InitializerContainer
from test.unit.mocks.game_mock import GameMock
import pymasep as pm


def test_apply(mock_environment):
    game = GameMock()

    initializer = InitializerContainer(game, {'name': 'OneInitializerContainer',
                                              'type': 'ALL',
                                              'obj_init': {'object1': {'cost': 0,
                                                                       'template': 'DefaultObjectTemplate',
                                                                       'initializer': 'objectinitializer'}}})
    container = pm.common.Container(environment=mock_environment, name='container',
                                    template=mock_environment.game.templates['ContainerTemplate'])
    assert container.state == 'init'

    initializer.apply(container)

    assert container.state == 'run'
    assert len(container) == 1
    assert container['object1'].name == 'object1'
    assert container['object1'].object_state.characteristics['CharacInt'].value == 1


def test_apply_with_params(mock_environment):
    game = GameMock()

    initializer = InitializerContainer(game, {'name': 'OneInitializerContainer',
                                              'type': 'BUY',
                                              'obj_init': {'object1': {'cost': 10,
                                                                       'template': 'DefaultObjectTemplate',
                                                                       'initializer': 'objectinitializer'},
                                                           'object2': {'cost': 10,
                                                                       'template': 'DefaultObjectTemplate',
                                                                       'initializer': 'object2initializer'}},
                                              })

    container = pm.common.Container(environment=mock_environment, name='container',
                                    template=mock_environment.game.templates['ContainerTemplate'])
    assert container.state == 'init'

    initializer.apply_with_params(container, ['object2'])

    assert container.state == 'run'
    assert len(container) == 1
    assert container['object2'].name == 'object2'
    assert container['object2'].object_state.characteristics['CharacInt'].value == 2

def test_apply_with_params_params_empty(mock_environment):
    game = GameMock()

    initializer = InitializerContainer(game, {'name': 'OneInitializerContainer',
                                              'type': 'BUY',
                                              'obj_init': {'object1': {'cost': 10,
                                                                       'template': 'DefaultObjectTemplate',
                                                                       'initializer': 'objectinitializer'},
                                                           'object2': {'cost': 10,
                                                                       'template': 'DefaultObjectTemplate',
                                                                       'initializer': 'object2initializer'}},
                                              })

    container = pm.common.Container(environment=mock_environment, name='container',
                                    template=mock_environment.game.templates['ContainerTemplate'])
    assert container.state == 'init'

    initializer.apply_with_params(container, None)

    assert container.state == 'run'
    assert len(container) == 0

def test_apply_with_params_params_list_empty(mock_environment):
    game = GameMock()

    initializer = InitializerContainer(game, {'name': 'OneInitializerContainer',
                                              'type': 'BUY',
                                              'obj_init': {'object1': {'cost': 10,
                                                                       'template': 'DefaultObjectTemplate',
                                                                       'initializer': 'objectinitializer'},
                                                           'object2': {'cost': 10,
                                                                       'template': 'DefaultObjectTemplate',
                                                                       'initializer': 'object2initializer'}},
                                              })

    container = pm.common.Container(environment=mock_environment, name='container',
                                    template=mock_environment.game.templates['ContainerTemplate'])
    assert container.state == 'init'

    initializer.apply_with_params(container, [])

    assert container.state == 'run'
    assert len(container) == 0
