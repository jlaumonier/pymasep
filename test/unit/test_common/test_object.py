from xml.etree.ElementTree import tostring, fromstring

import pytest

import pymasep as pm
from pymasep.common.exception import CreationException


def test_init_no_container(mock_environment):
    expected_name = 'Object1'

    obj = pm.common.Object(environment=mock_environment, name=expected_name,
                           template=mock_environment.game.templates['DefaultObjectTemplate'])

    assert isinstance(obj, pm.common.BaseObject)
    assert isinstance(obj, pm.common.Object)
    assert obj.environment == mock_environment
    assert obj.name == expected_name
    assert obj.object_state is not None
    assert isinstance(obj.object_state, pm.common.ObjectState)
    assert obj.object_state.get_fullname() == expected_name + '.state'


def test_to_xml_transformed(mock_environment):
    expected_xml_str = '<Object name="object1" state="run">' \
                       '<ObjectState name="state" state="run">' \
                       '<Characteristic name="Step0" state="run"><int>0</int></Characteristic>' \
                       '</ObjectState>' \
                       '</Object>'
    obj = pm.common.Object(environment=mock_environment, name='object1',
                           template=mock_environment.game.templates['EmptyObjectTemplate'])
    charac = pm.common.Characteristic(environment=mock_environment, name='Step0',
                                      template=mock_environment.game.templates['CharacInt'])
    charac.value = 0
    obj.object_state.add_characteristic(charac)

    xml_str_result = tostring(obj.to_xml(), encoding='unicode')

    assert xml_str_result == expected_xml_str


def test_to_xml_transformed_init(mock_environment):
    expected_xml_str = '<Object name="object1" state="init">' \
                       '<ObjectState name="state" state="init">' \
                       '<Characteristic name="Step0" state="init"><int /></Characteristic>' \
                       '</ObjectState>' \
                       '</Object>'
    obj = pm.common.Object(environment=mock_environment,
                           name='object1',
                           template=mock_environment.game.templates['EmptyObjectTemplate'])
    charac = pm.common.Characteristic(environment=mock_environment, name='Step0',
                                      template=mock_environment.game.templates['CharacInt'])
    charac.state = 'init'
    obj.object_state.add_characteristic(charac)

    xml_str_result = tostring(obj.to_xml(), encoding='unicode')

    assert xml_str_result == expected_xml_str


def test_to_xml_container(mock_environment):
    expected_xml_str = '<Object name="obj_container1" state="init">' \
                       '<ObjectState name="state" state="run">' \
                       "<Characteristic name=\"CharacInt\" state=\"init\"><int /></Characteristic>" \
                       '<Characteristic name="CharacStr" state=\"init\"><str /></Characteristic>' \
                       '</ObjectState>' \
                       '<Container name="container1" state="init" initializer="OneInitializerContainerIntf">' \
                       '<Object name="object1" state="run">' \
                       '<ObjectState name="state" state="run">' \
                       '<Characteristic name="Step0" state="run"><int>10</int></Characteristic>' \
                       '</ObjectState>' \
                       '</Object>' \
                       '<Object name="object2" state="init">' \
                       '<ObjectState name="state" state="init" />' \
                       '</Object>' \
                       '</Container>' \
                       '</Object>'
    obj_container1 = pm.common.Object(environment=mock_environment, name='obj_container1',
                                      template=mock_environment.game.templates['ContainerObjectTemplate'])
    obj_container1.containers['container1'].initializer = mock_environment.game.initializers['OneInitializerContainerIntf']

    obj1 = pm.common.Object(environment=mock_environment, name='object1',
                            template=mock_environment.game.templates['EmptyObjectTemplate'])
    step0 = pm.common.Characteristic(environment=mock_environment, name='Step0',
                                     template=mock_environment.game.templates['CharacInt'])
    step0.value = 10
    obj1.object_state.add_characteristic(step0)
    obj2 = pm.common.Object(environment=mock_environment, name='object2',
                            template=mock_environment.game.templates['EmptyObjectTemplate'])

    obj_container1.add_object_in_container(obj1, 'container1')
    obj_container1.add_object_in_container(obj2, 'container1')

    xml_str_result = tostring(obj_container1.to_xml(), encoding='unicode')

    assert xml_str_result == expected_xml_str


def test_from_xml_creation(mock_environment):
    object_xml_str = '<Object name="object1">' \
                     '<ObjectState name="state">' \
                     '<Characteristic name="Step0"><int>0</int></Characteristic>' \
                     '</ObjectState>' \
                     '</Object>'
    xml_node = fromstring(object_xml_str)
    obj = mock_environment.create_base_object(name='', xml_node=xml_node)

    obj.from_xml(environment=mock_environment, xml_node=xml_node)

    assert obj.name == 'object1'
    assert isinstance(obj.object_state, pm.common.ObjectState)
    assert isinstance(obj.object_state.characteristics['Step0'], pm.common.Characteristic)
    assert obj.state == 'run'


def test_from_xml_creation_with_container(mock_environment):
    object_xml_str = '<Object name="object1">' \
                     '<ObjectState name="state">' \
                     '<Characteristic name="Step0"><int>0</int></Characteristic>' \
                     '</ObjectState>' \
                     '<Container name="container1" state="run">' \
                     '<Object name="object1" state="run">' \
                     '<ObjectState name="state" state="run">' \
                     '<Characteristic name="Step0" state="run"><int>10</int></Characteristic>' \
                     '</ObjectState>' \
                     '</Object>' \
                     '<Object name="object2" state="run">' \
                     '<ObjectState name="state" state="run">' \
                     '<Characteristic name="Step1" state="run"><int>9</int></Characteristic>' \
                     '</ObjectState>' \
                     '</Object>' \
                     '</Container>' \
                     '</Object>'
    xml_node = fromstring(object_xml_str)
    obj = mock_environment.create_base_object(name='', xml_node=xml_node)

    obj.from_xml(environment=mock_environment, xml_node=xml_node)

    assert obj.name == 'object1'
    assert len(obj.containers['container1']) == 2


def test_from_xml_creation_with_container_with_state_run(mock_environment):
    object_xml_str = '<Object name="object1" state="run">' \
                     '<ObjectState name="state">' \
                     '<Characteristic name="Step0"><int>0</int></Characteristic>' \
                     '</ObjectState>' \
                     '<Container name="container1" state="run">' \
                     '<Object name="object1" state="run">' \
                     '<ObjectState name="state" state="run">' \
                     '<Characteristic name="Step0" state="run"><int>10</int></Characteristic>' \
                     '</ObjectState>' \
                     '</Object>' \
                     '<Object name="object2" state="run">' \
                     '<ObjectState name="state" state="run">' \
                     '<Characteristic name="Step1" state="run"><int>9</int></Characteristic>' \
                     '</ObjectState>' \
                     '</Object>' \
                     '</Container>' \
                     '</Object>'
    xml_node = fromstring(object_xml_str)
    obj = mock_environment.create_base_object(name='', xml_node=xml_node)

    obj.from_xml(environment=mock_environment, xml_node=xml_node)

    assert obj.name == 'object1'
    assert len(obj.containers['container1']) == 2


def test_from_xml_with_template_only_creation(mock_environment):
    object_xml_str = '<Object name="object1" template="DefaultObjectTemplate"/>'
    xml_node = fromstring(object_xml_str)
    obj = mock_environment.create_base_object(xml_node=xml_node)

    try:
        obj.from_xml(environment=mock_environment, xml_node=xml_node)
    except CreationException as e:
        assert str(e) == 'SubElement must be present if no initializer not present'
    else:
        assert False


def test_from_xml_with_template_initializer_creation_container_run(mock_environment):
    object_xml_str = '<Object name="object1" template="ContainerObjectTemplate" initializer="objectinitializercontainer"/>'
    xml_node = fromstring(object_xml_str)
    obj = mock_environment.create_base_object(xml_node=xml_node)

    obj.from_xml(environment=mock_environment, xml_node=xml_node)

    assert obj.state == 'run'
    assert isinstance(obj.object_state, pm.common.ObjectState)
    assert len(obj.containers) == 1
    assert len(obj.containers['container1']) == 0


def test_from_xml_with_template_initializer_creation(mock_environment):
    object_xml_str = '<Object name="object1" template="DefaultObjectTemplate" initializer="objectinitializer"/>'
    xml_node = fromstring(object_xml_str)
    obj = mock_environment.create_base_object(xml_node=xml_node)

    obj.from_xml(environment=mock_environment, xml_node=xml_node)

    assert obj.state == 'run'
    assert isinstance(obj.object_state, pm.common.ObjectState)
    assert len(obj.object_state.characteristics) == 2
    assert obj.object_state.characteristics['CharacInt'].value == 1
    assert obj.object_state.characteristics['CharacStr'].value == 'OneValue'


def test_from_xml_with_template_initializer_charac_creation(mock_environment):
    object_xml_str = '<Object name="object1" template="DefaultObjectTemplate" initializer="objectinitializer">' \
                     '<ObjectState name="state">' \
                     '<Characteristic name="Step0"><int>1</int></Characteristic>' \
                     '</ObjectState>' \
                     '</Object>'
    xml_node = fromstring(object_xml_str)
    obj = mock_environment.create_base_object(xml_node=xml_node)

    obj.from_xml(environment=mock_environment, xml_node=xml_node)

    assert obj.state == 'run'
    assert isinstance(obj.object_state, pm.common.ObjectState)
    assert isinstance(obj.object_state.characteristics['CharacInt'], pm.common.Characteristic)
    assert isinstance(obj.object_state.characteristics['CharacStr'], pm.common.Characteristic)
    assert isinstance(obj.object_state.characteristics['Step0'], pm.common.Characteristic)
    assert obj.object_state.characteristics['Step0'].value == 1


def test_from_xml_with_template_and_charac_missing_some_initializer(mock_environment):
    object_xml_str = '<Object name="object1" template="DefaultObjectTemplate">' \
                     '<ObjectState name="state">' \
                     '<Characteristic name="Step0"><int>1</int></Characteristic>' \
                     '</ObjectState>' \
                     '</Object>'
    xml_node = fromstring(object_xml_str)
    obj = mock_environment.create_base_object(xml_node=xml_node)

    try:
        obj.from_xml(environment=mock_environment, xml_node=xml_node)
    except CreationException as e:
        assert str(e) == 'Missing initializer for CharacInt characteristic'
    else:
        assert False


def test_from_xml_init_initializer_interface(mock_environment):
    object_xml_str = '<Object name="object1" state="init" initializer="objectinitializerinterface">' \
                     '<ObjectState name="state" state="init">' \
                     '<Characteristic name="CharacInt" state="run"><int>10</int></Characteristic>' \
                     '<Characteristic name="CharacStr" state="init"><str /></Characteristic>' \
                     '<Characteristic name="Step0" state="run"><int>1</int></Characteristic>' \
                     '</ObjectState>' \
                     '</Object>'
    xml_node = fromstring(object_xml_str)
    obj = mock_environment.create_base_object(xml_node=xml_node)

    obj.from_xml(environment=mock_environment, xml_node=xml_node)

    assert obj.state == 'init'
    assert obj.initializer == mock_environment.game.initializers['objectinitializerinterface']
    assert obj.object_state.characteristics['CharacInt'].value == 10
    assert obj.object_state.characteristics['CharacInt'].value_type == int
    assert obj.object_state.characteristics['CharacStr'].value is None
    assert obj.object_state.characteristics['CharacStr'].value_type == str
    assert obj.object_state.characteristics['Step0'].value == 1
    assert obj.object_state.characteristics['Step0'].value_type == int


def test_from_xml_state_obj_state_run_should_run(mock_environment):
    object_xml_str = '<Object name="object1" state="run">' \
                     '<ObjectState name="state" state="run">' \
                     '<Characteristic name="Step0" state="run"><int>0</int></Characteristic>' \
                     '</ObjectState>' \
                     '</Object>'
    xml_node = fromstring(object_xml_str)
    obj = mock_environment.create_base_object(name='', xml_node=xml_node)

    obj.from_xml(environment=mock_environment, xml_node=xml_node)

    assert obj.state == 'run'


def test_state_obj_state_init_should_be_init(mock_environment):
    object_xml_str = '<Object name="object1" state="init">' \
                     '<ObjectState name="state" state="init" initializer="DummyInitializer"/>' \
                     '</Object>'
    xml_node = fromstring(object_xml_str)
    obj = mock_environment.create_base_object(name='', xml_node=xml_node)

    obj.from_xml(environment=mock_environment, xml_node=xml_node)

    assert obj.state == 'init'

def test_state_container_init_should_be_init(mock_environment):
    object_xml_str = '<Object name="object1" state="init">' \
                     '<ObjectState name="state" state="run">' \
                     '<Characteristic name="Step0" state="run"><int>0</int></Characteristic>' \
                     '</ObjectState>' \
                     '<Container name="container1" state="init" initializer="DummyInitializer"/>' \
                     '</Object>'
    xml_node = fromstring(object_xml_str)
    obj = mock_environment.create_base_object(name='', xml_node=xml_node)

    obj.from_xml(environment=mock_environment, xml_node=xml_node)

    assert obj.state == 'init'


def test_state_container_run_should_be_run(mock_environment):
    object_xml_str = '<Object name="object1" state="init">' \
                     '<ObjectState name="state" state="run">' \
                     '<Characteristic name="Step0" state="run"><int>0</int></Characteristic>' \
                     '</ObjectState>' \
                     '<Container name="container1" state="run"/>' \
                     '</Object>'
    xml_node = fromstring(object_xml_str)
    obj = mock_environment.create_base_object(name='', xml_node=xml_node)

    obj.from_xml(environment=mock_environment, xml_node=xml_node)

    assert obj.state == 'run'


def test_id_str(mock_environment):
    object_xml_str = '<Object name="object1" state="run">' \
                     '<ObjectState name="state" state="run">' \
                     '<Characteristic name="Step0" state="run"><int>0</int></Characteristic>' \
                     '</ObjectState>' \
                     '<Container name="container1" state="run">' \
                     '<Object name="object1" state="run">' \
                     '<ObjectState name="state" state="run">' \
                     '<Characteristic name="Step0" state="run"><int>10</int></Characteristic>' \
                     '</ObjectState>' \
                     '</Object>' \
                     '</Container>' \
                     '</Object>'

    xml_node = fromstring(object_xml_str)
    obj = mock_environment.create_base_object(name='', xml_node=xml_node)

    obj.from_xml(environment=mock_environment, xml_node=xml_node)

    id_str = obj.id_str()

    assert id_str == 'object1stateStep00container1object1stateStep010'


def test_init_with_container(mock_environment):
    expected_name = 'Object1'

    obj = pm.common.Object(environment=mock_environment, name=expected_name,
                           template=mock_environment.game.templates['ContainerObjectTemplate'])

    assert isinstance(obj, pm.common.BaseObject)
    assert isinstance(obj, pm.common.Object)
    assert isinstance(obj.object_state, pm.common.ObjectState)
    assert obj.object_state.get_fullname() == expected_name + '.state'
    assert len(obj.containers) == 1
    assert isinstance(obj.containers['container1'], pm.common.Container)
    assert obj.containers['container1'].get_fullname() == expected_name + '.container1'


def test_init_with_container_wrong_name(mock_environment):
    try:
        _ = pm.common.Object(environment=mock_environment, name='Object1',
                             template=mock_environment.game.templates['BadNameContainerObjectTemplate'])
    except CreationException as e:
        assert str(e) == "The container cannot be named 'state'"
    else:
        assert False


def test_add_object_in_container(mock_environment):
    obj_container = pm.common.Object(environment=mock_environment, name='bag',
                                     template=mock_environment.game.templates['ContainerObjectTemplate'])
    obj = pm.common.Object(environment=mock_environment, name='object1',
                           template=mock_environment.game.templates['DefaultObjectTemplate'])

    obj_container.add_object_in_container(obj, 'container1')

    assert obj in obj_container.containers['container1']
    assert obj.parent == obj_container.containers['container1']


def test_remove_object_from_container(mock_environment):
    obj_container = pm.common.Object(environment=mock_environment, name='bag',
                                     template=mock_environment.game.templates['ContainerObjectTemplate'])
    obj = pm.common.Object(environment=mock_environment, name='object1',
                           template=mock_environment.game.templates['DefaultObjectTemplate'])
    obj_container.add_object_in_container(obj, 'container1')
    assert obj in obj_container.containers['container1']

    obj_container.remove_object_from_container(obj)

    assert obj not in obj_container.containers['container1']
    assert obj.parent is None

@pytest.mark.parametrize('obj_xml_str',
                         [('<Object name="object1" state="run">'
                           '<ObjectState name="state" state="run">'
                           '<Characteristic name="Step0" state="run"><int>0</int></Characteristic>'
                           '</ObjectState>'
                           '<Container name="container1" state="run">'
                           '<Object name="object1" state="run">'
                           '<ObjectState name="state" state="run ">'
                           '<Characteristic name="Step0" state="run"><int>10</int></Characteristic>'
                           '</ObjectState>'
                           '</Object>'
                           '</Container>'
                           '</Object>')])
def test_copy_obs(mock_environment, obj_xml_str):
    xml_node = fromstring(obj_xml_str)
    obj= mock_environment.create_object(name='', xml_node=xml_node)

    result = obj.copy_obs(params={})

    assert result != obj
    assert len(result.object_state.characteristics) == len(obj.object_state.characteristics)
    assert result.object_state != obj.object_state
    assert result.object_state.parent == result
    assert result.containers['container1'].id_str() == obj.containers['container1'].id_str()
    assert result.containers['container1'] != obj.containers['container1']
    assert result.containers['container1'].parent == result

