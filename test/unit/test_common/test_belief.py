from xml.etree.ElementTree import tostring, fromstring

import pytest
import networkx as nx
import matplotlib

from networkx_query import search_direct_relationships, search_nodes

import pymasep as pm
from pymasep.common import Belief


def test_init(mock_environment):
    template = mock_environment.game.templates['BeliefTemplate']

    belief = Belief(environment=mock_environment, name='belief1', template=template, xml_node=None, parent=None)

    assert belief.name == 'belief1'
    assert len(belief.beliefs.nodes) == 0


def test_id_str(mock_environment):
    template = mock_environment.game.templates['BeliefTemplate']
    belief = Belief(environment=mock_environment, name='belief1', template=template, xml_node=None, parent=None)

    # agent a1 told sentence s1
    belief.add(c1=('agent', 'ag1'), r=('tell',), c2=('sentence', 's1'))

    assert belief.id_str() == 'belief1dbdd5be642a18e3ede90487502d0d29f'


def test_add_belief(mock_environment):
    template = mock_environment.game.templates['BeliefTemplate']
    belief = Belief(environment=mock_environment, name='belief1', template=template, xml_node=None, parent=None)

    # agent a1 told sentence s1
    belief.add(c1=('agent', 'ag1'), r=('tell',), c2=('sentence', 's1'))

    assert list(belief.beliefs.nodes(data=True)) == [(1, {'c': 'agent', 'v': 'ag1'}), (2, {'c': 'tell'}),
                                                     (3, {'c': 'sentence', 'v': 's1'})]
    assert list(belief.beliefs.edges()) == [(1, 2), (2, 3)]


def test_add_belief_two_mergeable_belief_by_agent(mock_environment):
    template = mock_environment.game.templates['BeliefTemplate']
    belief = Belief(environment=mock_environment, name='belief1', template=template, xml_node=None, parent=None)

    # agent a1 told sentence s1
    belief.add(c1=('agent', 'ag1'), r=('tell',), c2=('sentence', 's1'))
    # agent a1 told also sentence s2
    belief.add(c1=('agent', 'ag1'), r=('tell',), c2=('sentence', 's2'))

    assert list(belief.beliefs.nodes(data=True)) == [(1, {'c': 'agent', 'v': 'ag1'}), (2, {'c': 'tell'}),
                                                     (3, {'c': 'sentence', 'v': 's1'}),
                                                     (4, {'c': 'sentence', 'v': 's2'})]
    assert list(belief.beliefs.edges()) == [(1, 2), (2, 3), (2, 4)]

def test_add_belief_two_mergeable_belief_by_sentence(mock_environment):
    template = mock_environment.game.templates['BeliefTemplate']
    belief = Belief(environment=mock_environment, name='belief1', template=template, xml_node=None, parent=None)

    # agent a1 told sentence s1
    belief.add(c1=('agent', 'ag1'), r=('tell',), c2=('sentence', 's1'))
    # agent a2 told also sentence s1
    belief.add(c1=('agent', 'ag2'), r=('tell',), c2=('sentence', 's1'))

    assert list(belief.beliefs.nodes(data=True)) == [(1, {'c': 'agent', 'v': 'ag1'}), (2, {'c': 'tell'}),
                                                     (3, {'c': 'sentence', 'v': 's1'}),
                                                     (4, {'c': 'agent', 'v': 'ag2'})]
    assert list(belief.beliefs.edges()) == [(1, 2), (2, 3), (2, 4)]


def test_add_belief_two_not_so_mergeable_belief(mock_environment):
    template = mock_environment.game.templates['BeliefTemplate']
    belief = Belief(environment=mock_environment, name='belief1', template=template, xml_node=None, parent=None)

    # agent a1 told sentence s1
    belief.add(c1=('agent', 'ag1'), r=('tell',), c2=('sentence', 's1'))
    # agent a2 told sentence s2
    belief.add(c1=('agent', 'ag2'), r=('tell',), c2=('sentence', 's2'))

    assert list(belief.beliefs.nodes(data=True)) == [(1, {'c': 'agent', 'v': 'ag1'}), (2, {'c': 'tell'}),
                                                     (3, {'c': 'sentence', 'v': 's1'}),
                                                     (4, {'c': 'agent', 'v': 'ag2'}),
                                                     (5, {'c': 'tell'}),
                                                     (6, {'c': 'sentence', 'v': 's2'})]
    assert list(belief.beliefs.edges()) == [(1, 2), (2, 3), (4, 5), (5, 6)]


def test_query_empty_belief(mock_environment):
    template = mock_environment.game.templates['BeliefTemplate']
    belief = Belief(environment=mock_environment, name='belief1', template=template, xml_node=None, parent=None)
    query = belief._create_request(c1=('agent', '*'), r=('tell',), c2=('sentence', '*'))

    result = belief.query(query)

    assert list(result.nodes(data=True)) == []
    assert list(result.edges()) == []

def test_query_no_result(mock_environment):
    template = mock_environment.game.templates['BeliefTemplate']
    belief = Belief(environment=mock_environment, name='belief1', template=template, xml_node=None, parent=None)
    belief.add(c1=('agent', 'ag1'), r=('tell',), c2=('sentence', 's1'))
    query = belief._create_request(c1=('agent', 'ag2'), r=('tell',), c2=('sentence', '*'))
    
    result = belief.query(query)

    assert list(result.nodes(data=True)) == []
    assert list(result.edges()) == []


def test_query_belief(mock_environment):
    template = mock_environment.game.templates['BeliefTemplate']
    belief = Belief(environment=mock_environment, name='belief1', template=template, xml_node=None, parent=None)
    # agent a1 told sentence s1
    belief.add(c1=('agent', 'ag1'), r=('tell',), c2=('sentence', 's1'))

    query = belief._create_request(c1=('agent', '*'), r=('tell',), c2=('sentence', '*'))

    result = belief.query(query)

    assert list(result.nodes(data=True)) == [(1, {'c': 'agent', 'v': 'ag1'}), (2, {'c': 'tell'}),
                                             (3, {'c': 'sentence', 'v': 's1'})]
    assert list(result.edges()) == [(1, 2), (2, 3)]


def test_query_two_beliefs(mock_environment):
    template = mock_environment.game.templates['BeliefTemplate']
    belief = Belief(environment=mock_environment, name='belief1', template=template, xml_node=None, parent=None)
    # agent a1 told sentence s1
    belief.add(c1=('agent', 'ag1'), r=('tell',), c2=('sentence', 's1'))
    # agent a1 told also sentence s2
    belief.add(c1=('agent', 'ag1'), r=('tell',), c2=('sentence', 's2'))

    query = belief._create_request(c1=('agent', '*'), r=('tell',), c2=('sentence', '*'))

    result = belief.query(query)

    assert list(result.nodes(data=True)) == [(1, {'c': 'agent', 'v': 'ag1'}), (2, {'c': 'tell'}),
                                                     (3, {'c': 'sentence', 'v': 's1'}),
                                                     (4, {'c': 'sentence', 'v': 's2'})]
    assert list(result.edges()) == [(1, 2), (2, 3), (2, 4)]

def test_query_two_beliefs_merged(mock_environment):
    template = mock_environment.game.templates['BeliefTemplate']
    belief = Belief(environment=mock_environment, name='belief1', template=template, xml_node=None, parent=None)
    # agent a1 told sentence s1
    belief.add(c1=('agent', 'ag1'), r=('tell',), c2=('sentence', 's1'))
    # agent a2 told sentence s2
    belief.add(c1=('agent', 'ag2'), r=('tell',), c2=('sentence', 's2'))

    query = belief._create_request(c1=('agent', 'ag1'), r=('tell',), c2=('sentence', '*'))

    result = belief.query(query)

    assert list(result.nodes(data=True)) == [(1, {'c': 'agent', 'v': 'ag1'}), (2, {'c': 'tell'}),
                                                     (3, {'c': 'sentence', 'v': 's1'})]
    assert list(result.edges()) == [(1, 2), (2, 3)]


def test_to_xml(mock_environment):
    expected_xml_str = ('<Belief name="belief1" state="run">'
                        '<content>&lt;graphml '
                        'xmlns="http://graphml.graphdrawing.org/xmlns" '
                        'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '
                        'xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns '
                        'http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd"&gt;  &lt;key id="d1" '
                        'for="node" attr.name="v" attr.type="string" /&gt;  &lt;key id="d0" '
                        'for="node" attr.name="c" attr.type="string" /&gt;  &lt;graph '
                        'edgedefault="undirected"&gt;    &lt;node id="1"&gt;      &lt;data '
                        'key="d0"&gt;agent&lt;/data&gt;      &lt;data key="d1"&gt;ag1&lt;/data&gt;    '
                        '&lt;/node&gt;    &lt;node id="2"&gt;      &lt;data '
                        'key="d0"&gt;tell&lt;/data&gt;    &lt;/node&gt;    &lt;node id="3"&gt;      '
                        '&lt;data key="d0"&gt;sentence&lt;/data&gt;      &lt;data '
                        'key="d1"&gt;s1&lt;/data&gt;    &lt;/node&gt;    &lt;edge source="1" '
                        'target="2" /&gt;    &lt;edge source="2" target="3" /&gt;  '
                        '&lt;/graph&gt;&lt;/graphml&gt;</content></Belief>')

    template = mock_environment.game.templates['BeliefTemplate']
    belief = Belief(environment=mock_environment, name='belief1', template=template, xml_node=None, parent=None)
    # agent a1 told sentence s1
    belief.add(c1=('agent', 'ag1'), r=('tell',), c2=('sentence', 's1'))

    xml_str_result = tostring(belief.to_xml(), encoding='unicode')

    assert xml_str_result == expected_xml_str


def test_from_xml(mock_environment):
    xml_str = ('<Belief name="belief1" state="run">'
               '<content>'
               '&lt;graphml '
               'xmlns="http://graphml.graphdrawing.org/xmlns" '
               'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '
               'xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns '
               'http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd"&gt;  &lt;key id="d1" '
               'for="node" attr.name="v" attr.type="string" /&gt;  &lt;key id="d0" '
               'for="node" attr.name="c" attr.type="string" /&gt;  &lt;graph '
               'edgedefault="undirected"&gt;    &lt;node id="1"&gt;      &lt;data '
               'key="d0"&gt;agent&lt;/data&gt;      &lt;data key="d1"&gt;ag1&lt;/data&gt;    '
               '&lt;/node&gt;    &lt;node id="2"&gt;      &lt;data '
               'key="d0"&gt;tell&lt;/data&gt;    &lt;/node&gt;    &lt;node id="3"&gt;      '
               '&lt;data key="d0"&gt;sentence&lt;/data&gt;      &lt;data '
               'key="d1"&gt;s1&lt;/data&gt;    &lt;/node&gt;    &lt;edge source="1" '
               'target="2" /&gt;    &lt;edge source="2" target="3" /&gt;  '
               '&lt;/graph&gt;&lt;/graphml&gt;'
               '</content></Belief>')

    xml_node = fromstring(xml_str)
    belief = mock_environment.create_base_object(name='', xml_node=xml_node)

    belief.from_xml(environment=mock_environment, xml_node=xml_node)

    assert belief.name == 'belief1'
    assert list(belief.beliefs.nodes(data=True)) == [(1, {'c': 'agent', 'v': 'ag1'}), (2, {'c': 'tell'}),
                                                     (3, {'c': 'sentence', 'v': 's1'})]
    assert list(belief.beliefs.edges()) == [(1, 2), (2, 3)]
