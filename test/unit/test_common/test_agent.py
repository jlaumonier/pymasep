from xml.etree.ElementTree import tostring, fromstring

import pytest

from pymasep.common.exception import CreationException
from pymasep.common import Characteristic, Agent, Intention, BaseObject, Object, State, ObjectState, Action, Belief
from test.unit.mocks.controller_mock import ControllerMock
from test.unit.mocks.game_mock import GameMock


def test_init(mock_environment):
    expected_name = 'AgentName1'
    ag = Agent(environment=mock_environment,
               name=expected_name,
               template=mock_environment.game.templates['DefaultAgentTemplate'])
    assert isinstance(ag, BaseObject)
    assert isinstance(ag, Object)
    assert isinstance(ag, Agent)
    assert expected_name == ag.name
    assert ag.action is None
    assert ag.observation is None
    assert ag.intention is None
    assert ag.reward is None
    assert len(ag.belief.beliefs.nodes) == 0


def test_choose_action(mock_environment):
    observation = State(environment=mock_environment,
                        name='state',
                        template=mock_environment.game.templates['DefaultStateTemplate'])
    expected_action_type = GameMock.ACTION_TYPE_2
    ag = Agent(environment=mock_environment,
               name='AgentName1',
               template=mock_environment.game.templates['DefaultAgentTemplate'])
    ag.controller = ControllerMock(mock_environment)
    ag.observation = observation

    ag.choose_action()

    assert expected_action_type == ag.action.type


def test_to_xml_transformed(mock_environment):
    expected_xml_str = ('<Agent name="agent1" state="init" '
                        'controller="test.unit.mocks.controller_mock.ControllerMock" control="interface">'
                        '<ObjectState name="state" state="init">'
                        '<Characteristic name="CharacInt" state="init"><int /></Characteristic>'
                        '<Characteristic name="CharacStr" state="init"><str /></Characteristic>'
                        '<Characteristic name="Step0" state="run"><int>0</int></Characteristic>'
                        '</ObjectState>'
                        '<Intention name="intention" state="run">'
                        '<Action type="1" />'
                        '</Intention>'
                        '<Belief name="belief" state="run">'
                        '<content>'
                        '&lt;graphml '
                        'xmlns="http://graphml.graphdrawing.org/xmlns" '
                        'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '
                        'xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns '
                        'http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd"&gt;  '
                        '&lt;graph edgedefault="undirected" /&gt;&lt;/graphml&gt;'
                        '</content>'
                        '</Belief>'
                        '</Agent>')
    ag = Agent(environment=mock_environment,
               name='agent1',
               template=mock_environment.game.templates['DefaultAgentTemplate'])
    ag.controller = ControllerMock(mock_environment)
    ag.object_state.characteristics['CharacInt'].state = 'init'
    ag.object_state.characteristics['CharacStr'].state = 'init'
    charac = Characteristic(environment=mock_environment, name='Step0',
                            template=mock_environment.game.templates['CharacInt'],
                            parent=ag.object_state)
    charac.value = 0
    ag.object_state.add_characteristic(charac)
    ag.control = 'interface'
    ag.intention = Intention(environment=mock_environment,
                             name="intention",
                             template=mock_environment.game.templates['EmptyIntentionTemplate'])
    ag.intention.add_intention(Action(environment=mock_environment,
                                      action_type=mock_environment.game.ACTION_TYPE_1))
    xml_str_result = tostring(ag.to_xml(), encoding='unicode')

    assert xml_str_result == expected_xml_str


def test_from_xml(mock_environment):
    agent_xml_str = ('<Agent name="agent1" controller="test.mocks.controller_mock.ControllerMock">'
                     '<ObjectState name="state">'
                     '<Characteristic name="Step0"><int>0</int></Characteristic>'
                     '</ObjectState>'
                     '<Intention name="intention">'
                     '<Action type="1" />'
                     '</Intention>'
                     '<Belief name="belief" state="run">'
                     '<content>'
                     '&lt;graphml '
                     'xmlns="http://graphml.graphdrawing.org/xmlns" '
                     'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '
                     'xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns '
                     'http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd"&gt;  &lt;key id="d1" '
                     'for="node" attr.name="v" attr.type="string" /&gt;  &lt;key id="d0" '
                     'for="node" attr.name="c" attr.type="string" /&gt;  &lt;graph '
                     'edgedefault="undirected"&gt;    &lt;node id="1"&gt;      &lt;data '
                     'key="d0"&gt;agent&lt;/data&gt;      &lt;data key="d1"&gt;ag1&lt;/data&gt;    '
                     '&lt;/node&gt;    &lt;node id="2"&gt;      &lt;data '
                     'key="d0"&gt;tell&lt;/data&gt;    &lt;/node&gt;    &lt;node id="3"&gt;      '
                     '&lt;data key="d0"&gt;sentence&lt;/data&gt;      &lt;data '
                     'key="d1"&gt;s1&lt;/data&gt;    &lt;/node&gt;    &lt;edge source="1" '
                     'target="2" /&gt;    &lt;edge source="2" target="3" /&gt;  '
                     '&lt;/graph&gt;&lt;/graphml&gt;'
                     '</content>'
                     '</Belief>'
                     '</Agent>')
    xml_node = fromstring(agent_xml_str)
    ag = mock_environment.create_base_object(name='', xml_node=xml_node)

    ag.from_xml(environment=mock_environment, xml_node=xml_node)

    assert isinstance(ag, Agent)
    assert ag.name == 'agent1'
    assert isinstance(ag.object_state, ObjectState)
    assert isinstance(ag.object_state.characteristics['Step0'], Characteristic)
    assert len(ag.intention.intentions) == 1
    assert isinstance(ag.intention.intentions[0], Action)
    assert ag.intention.intentions[0].type == 1
    assert len(ag.belief.beliefs.nodes) == 3


def test_from_xml_control_interface(mock_environment):
    agent_xml_str = '<Agent name="agent1" control="interface">' \
                    '<ObjectState name="state">' \
                    '<Characteristic name="Step0"><int>0</int></Characteristic>' \
                    '</ObjectState>' \
                    '</Agent>'
    xml_node = fromstring(agent_xml_str)
    ag = mock_environment.create_base_object(name='', xml_node=xml_node)

    ag.from_xml(environment=mock_environment, xml_node=xml_node)

    assert ag.control == 'interface'


def test_from_xml_with_template_only_creation(mock_environment):
    agent_xml_str = '<Agent name="agent1" control="interface" template="DefaultAgentTemplate" />'
    xml_node = fromstring(agent_xml_str)
    ag = mock_environment.create_base_object(name='', xml_node=xml_node)

    try:
        ag.from_xml(environment=mock_environment, xml_node=xml_node)
    except CreationException as e:
        assert str(e) == 'SubElement must be present if no initializer not present'
    else:
        assert False


def test_from_xml_with_template_initializer_creation_container_run(mock_environment):
    agent_xml_str = ('<Agent name="agent1" control="interface" '
                     'template="DefaultAgentTemplate" initializer="objectinitializer"/>')
    xml_node = fromstring(agent_xml_str)
    ag = mock_environment.create_base_object(name='', xml_node=xml_node)

    ag.from_xml(environment=mock_environment, xml_node=xml_node)

    assert isinstance(ag, Agent)
    assert ag.name == 'agent1'
    assert len(ag.belief.beliefs.nodes) == 0


def test_id_str(mock_environment):
    agent_xml_str = '<Agent name="agent1" controller="test.mocks.controller_mock.ControllerMock">' \
                    '<ObjectState name="state">' \
                    '<Characteristic name="Step0"><int>0</int></Characteristic>' \
                    '</ObjectState>' \
                    '<Intention name="intention">' \
                    '<Action type="1" />' \
                    '</Intention>' \
                    '</Agent>'
    xml_node = fromstring(agent_xml_str)
    ag = mock_environment.create_base_object(name='', xml_node=xml_node)
    ag.from_xml(environment=mock_environment, xml_node=xml_node)

    id_str = ag.id_str()

    assert id_str == 'agent1stateStep00intentionaction1beliefa3ff5ca398dbcdfa31fe88d1ea3607a5'


@pytest.mark.parametrize('agnt_xml_str, obs_agent_fname',
                         [('<Agent name="agent1" controller="test.unit.mocks.controller_mock.ControllerMock">'
                           '<ObjectState name="state">'
                           '<Characteristic name="Step0"><int>0</int></Characteristic>'
                           '</ObjectState>'
                           '<Intention name="intention">'
                           '<Action type="1" />'
                           '</Intention>'
                           '<Belief name="belief" state="run">'
                           '<content>'
                           '&lt;graphml '
                           'xmlns="http://graphml.graphdrawing.org/xmlns" '
                           'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '
                           'xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns '
                           'http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd"&gt;  '
                           '&lt;graph edgedefault="undirected" /&gt;&lt;/graphml&gt;'
                           '</content>'
                           '</Belief>'
                           '</Agent>', 'agent1'),
                          ('<Agent name="agent1" controller="test.unit.mocks.controller_mock.ControllerMock">'
                           '<ObjectState name="state">'
                           '<Characteristic name="Step0"><int>0</int></Characteristic>'
                           '</ObjectState>'
                           '<Belief name="belief" state="run">'
                           '<content>'
                           '&lt;graphml '
                           'xmlns="http://graphml.graphdrawing.org/xmlns" '
                           'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '
                           'xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns '
                           'http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd"&gt;  '
                           '&lt;graph edgedefault="undirected" /&gt;&lt;/graphml&gt;'
                           '</content>'
                           '</Belief>'
                           '</Agent>', 'agent1'),
                          ('<Agent name="agent1" controller="test.unit.mocks.controller_mock.ControllerMock">'
                           '<ObjectState name="state">'
                           '<Characteristic name="Step0"><int>0</int></Characteristic>'
                           '</ObjectState>'
                           '<Intention name="intention">'
                           '<Action type="1" />'
                           '</Intention>'
                           '<Belief name="belief" state="run">'
                           '<content>'
                           '&lt;graphml '
                           'xmlns="http://graphml.graphdrawing.org/xmlns" '
                           'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '
                           'xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns '
                           'http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd"&gt;  &lt;key id="d1" '
                           'for="node" attr.name="v" attr.type="string" /&gt;  &lt;key id="d0" '
                           'for="node" attr.name="c" attr.type="string" /&gt;  &lt;graph '
                           'edgedefault="undirected"&gt;    &lt;node id="1"&gt;      &lt;data '
                           'key="d0"&gt;agent&lt;/data&gt;      &lt;data key="d1"&gt;ag1&lt;/data&gt;    '
                           '&lt;/node&gt;    &lt;node id="2"&gt;      &lt;data '
                           'key="d0"&gt;tell&lt;/data&gt;    &lt;/node&gt;    &lt;node id="3"&gt;      '
                           '&lt;data key="d0"&gt;sentence&lt;/data&gt;      &lt;data '
                           'key="d1"&gt;s1&lt;/data&gt;    &lt;/node&gt;    &lt;edge source="1" '
                           'target="2" /&gt;    &lt;edge source="2" target="3" /&gt;  '
                           '&lt;/graph&gt;&lt;/graphml&gt;</content>'
                           '</Belief>'
                           '</Agent>', 'agent2')
                          ])
def test_copy_obs(mock_environment, agnt_xml_str, obs_agent_fname):
    xml_node = fromstring(agnt_xml_str)
    ag = mock_environment.create_object(name='', xml_node=xml_node)

    result = ag.copy_obs(params={'obs_agt_fname': obs_agent_fname})

    assert result.control == ag.control
    assert result.controller == ag.controller
    assert result.object_state is not None
    assert result.belief is not None
    assert result != ag
    assert result.object_state != ag.object_state
    if result.intention is not None:
        assert result.intention != ag.intention
    else:
        assert ag.intention is None
    if obs_agent_fname == ag.get_fullname():
        assert result.belief == ag.belief
        assert result.id_str() == ag.id_str()
    else:
        assert len(ag.belief.beliefs.nodes) == 3
        assert id(result.belief) != id(ag.belief)
        assert len(result.belief.beliefs.nodes) == 0
        assert result.id_str() != ag.id_str()
