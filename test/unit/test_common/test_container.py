from xml.etree.ElementTree import tostring, fromstring

import pytest

import pymasep as pm
from pymasep.common.exception import CreationException, ConstraintException


def test_init(mock_environment):
    container = pm.common.Container(environment=mock_environment, name='container',
                                    template=mock_environment.game.templates['ContainerTemplate'])
    assert isinstance(container, pm.common.BaseObject)
    assert isinstance(container, pm.common.Container)
    assert container.environment == mock_environment
    assert container.name == 'container'


def test_id_str(mock_environment):
    container_xml = '<Container name="container1" state="run">' \
                    '<Object name="object1" state="run">' \
                    '<ObjectState name="state" state="run">' \
                    '<Characteristic name="Step0" state="run"><int>10</int></Characteristic>' \
                    '</ObjectState>' \
                    '</Object>' \
                    '</Container>'
    xml_node = fromstring(container_xml)
    cntr = mock_environment.create_base_object(name='', xml_node=xml_node)

    cntr.from_xml(environment=mock_environment, xml_node=xml_node)

    id_str = cntr.id_str()

    assert id_str == 'container1object1stateStep010'


def test_to_xml(mock_environment):
    expected_xml_str = "<Container name=\"container1\" state=\"run\">" \
                       '<Object name="object1" state="run">' \
                       '<ObjectState name="state" state="run">' \
                       '<Characteristic name="Step0" state="run"><int>10</int></Characteristic>' \
                       '</ObjectState>' \
                       '</Object>' \
                       '</Container>'
    container = pm.common.Container(environment=mock_environment, name='container1',
                                    template=mock_environment.game.templates['ContainerTemplate'])
    obj = pm.common.Object(environment=mock_environment,
                           name='object1',
                           template=mock_environment.game.templates['EmptyObjectTemplate'])
    charac = pm.common.Characteristic(environment=mock_environment, name='Step0',
                                      template=mock_environment.game.templates['CharacInt'])
    charac.value = 10
    obj.object_state.add_characteristic(charac)
    container.add(obj)
    container.set_run()

    xml_str_result = tostring(container.to_xml(), encoding='unicode')

    assert xml_str_result == expected_xml_str


def test_to_xml_init_no_initializer(mock_environment):
    container = pm.common.Container(environment=mock_environment, name='container1',
                                    template=mock_environment.game.templates['ContainerTemplate'],
                                    )
    try:
        _ = tostring(container.to_xml(), encoding='unicode')
    except ConstraintException as e:
        assert str(e) == 'A container not initialized must have an initializer before streaming'
    else:
        assert False


def test_to_xml_init_with_initializer(mock_environment):
    expected_xml_str = '<Container name="container1" state="init" initializer="OneInitializerContainer" />'
    container = pm.common.Container(environment=mock_environment, name='container1',
                                    template=mock_environment.game.templates['ContainerTemplate'],
                                    )
    container.initializer = mock_environment.game.initializers['OneInitializerContainer']

    xml_str_result = tostring(container.to_xml(), encoding='unicode')

    assert xml_str_result == expected_xml_str


def test_from_xml_creation(mock_environment):
    object_xml_str = '<Container name="container1" state="run">' \
                     '<Object name="object1" state="run">' \
                     '<ObjectState name="state" state="run">' \
                     '<Characteristic name="Step0" state="run"><int>10</int></Characteristic>' \
                     '</ObjectState>' \
                     '</Object>' \
                     '</Container>'
    xml_node = fromstring(object_xml_str)
    cntn = mock_environment.create_base_object(name='', xml_node=xml_node)

    cntn.from_xml(environment=mock_environment, xml_node=xml_node)

    assert cntn.name == 'container1'
    assert isinstance(cntn, pm.common.Container)
    assert len(cntn) == 1
    assert cntn['object1'].name == 'object1'
    assert cntn.state == 'run'


def test_from_xml_simple_creation(mock_environment):
    object_xml_str = '<Container name="container1" initializer="OneInitializerContainer"/>'
    xml_node = fromstring(object_xml_str)
    cntn = mock_environment.create_base_object(name='', xml_node=xml_node)

    cntn.from_xml(environment=mock_environment, xml_node=xml_node)

    assert cntn.name == 'container1'
    assert isinstance(cntn, pm.common.Container)
    assert len(cntn) == 0
    assert cntn.state == 'init'  # if the state is not set in the XML the container is 'init'


def test_from_xml_creation_empty(mock_environment):
    object_xml_str = '<Container name="container1" state="run">' \
                     '<Object name="object1" state="run">' \
                     '<ObjectState name="state" state="init">' \
                     '<Characteristic name="Step0" state="init"><int/></Characteristic>' \
                     '</ObjectState>' \
                     '</Object>' \
                     '</Container>'

    xml_node = fromstring(object_xml_str)
    cntn = mock_environment.create_base_object(name='', xml_node=xml_node)

    try:
        cntn.from_xml(environment=mock_environment, xml_node=xml_node)
    except CreationException as e:
        assert str(e) == 'A container must be empty or containing only initialized object'
    else:
        assert False


def test_from_xml_simple_creation_init_without_initializer(mock_environment):
    object_xml_str = '<Container name="container1" state="init"/>'
    xml_node = fromstring(object_xml_str)
    cntn = mock_environment.create_base_object(name='', xml_node=xml_node)
    try:
        cntn.from_xml(environment=mock_environment, xml_node=xml_node)
    except CreationException as e:
        assert str(e) == 'A container not initialized must have an initializer'
    else:
        assert False


def test_from_xml_simple_creation_init(mock_environment):
    object_xml_str = '<Container name="container1" state="init" initializer="OneInitializerContainer"/>'
    xml_node = fromstring(object_xml_str)
    cntn = mock_environment.create_base_object(name='', xml_node=xml_node)

    cntn.from_xml(environment=mock_environment, xml_node=xml_node)

    assert cntn.name == 'container1'
    assert isinstance(cntn, pm.common.Container)
    assert len(cntn) == 0
    assert cntn.state == 'init'
    assert cntn.initializer is not None


@pytest.mark.parametrize('cntn_xml_str',
                         [('<Container name="container1" state="run">'
                           '<Object name="object1" state="run">'
                           '<ObjectState name="state" state="run">'
                           '<Characteristic name="Step0" state="run"><int>10</int></Characteristic>'
                           '</ObjectState>'
                           '</Object>'
                           '</Container>')])
def test_copy_obs(mock_environment, cntn_xml_str):
    xml_node = fromstring(cntn_xml_str)
    cntn = mock_environment.create_object(name='', xml_node=xml_node)

    result = cntn.copy_obs(params={})

    assert result != cntn
    assert len(result) == len(cntn)
    for o in result:
        assert o != cntn[o.name]
        assert o.id_str() == cntn[o.name].id_str()
        assert o.parent == result
