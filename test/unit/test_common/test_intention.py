from xml.etree.ElementTree import tostring, fromstring

import pytest

from pymasep.common import Intention, Environment, BaseObject, Action
from test.unit.mocks.game_mock import GameMock


def test_init(mock_environment):
    expected_name = 'intention'
    intention = Intention(environment=mock_environment, name='false_name',
                          template=mock_environment.game.templates['EmptyIntentionTemplate'])
    assert isinstance(intention, BaseObject)
    assert isinstance(intention, Intention)
    assert intention.environment == mock_environment
    assert intention.name == expected_name
    assert len(intention.intentions) == 0


def test_add_intention_intention_added(mock_environment):
    intention = Intention(environment=mock_environment, name='false_name',
                          template=mock_environment.game.templates['EmptyIntentionTemplate'])
    action = mock_environment.create_action(mock_environment.game.ACTION_TYPE_1)
    intention.add_intention(action)
    assert len(intention.intentions) == 1
    assert intention.intentions[0] == action


def test_to_xml_transformed(mock_environment):
    expected_xml_str = '<Intention name="intention" state="run">' \
                       '<Action type="1" />' \
                       '</Intention>'
    intention = Intention(environment=mock_environment, name='false_name',
                          template=mock_environment.game.templates['EmptyIntentionTemplate'])
    action = mock_environment.create_action(mock_environment.game.ACTION_TYPE_1)
    intention.add_intention(action)

    xml_str_result = tostring(intention.to_xml(), encoding='unicode')

    assert xml_str_result == expected_xml_str

def test_from_xml_no_template_should_init(mock_environment):
    intention_xml_str = '<Intention name="intention" state="run">' \
                        '<Action type="1" />' \
                        '</Intention>'
    xml_node = fromstring(intention_xml_str)

    intention = Intention(environment=mock_environment, xml_node=xml_node)
    intention.from_xml(mock_environment, xml_node)

    assert isinstance(intention, Intention)
    assert intention.name == 'intention'
    assert intention.parent is None
    assert len(intention.intentions) == 1
    assert isinstance(intention.intentions[0], Action)
    assert intention.intentions[0].type == 1

def test_id_str(mock_environment):
    intention_xml_str = '<Intention name="intention" state="run">' \
                        '<Action type="1" />' \
                        '</Intention>'
    xml_node = fromstring(intention_xml_str)
    intention = Intention(environment=mock_environment, xml_node=xml_node)
    intention.from_xml(mock_environment, xml_node)

    id_str = intention.id_str()

    assert id_str == 'intentionaction1'

@pytest.mark.parametrize('intention_xml_str',
                         [('<Intention name="intention" state="run"><Action type="1" /></Intention>')])
def test_copy_obs(mock_environment, intention_xml_str):
    xml_node = fromstring(intention_xml_str)
    intention = mock_environment.create_object(name='', xml_node=xml_node)

    result = intention.copy_obs(params={})

    assert result != intention
    assert len(result.intentions) == len(intention.intentions)
    for id_i, i in enumerate(intention.intentions):
        assert type(i) == type(result.intentions[id_i])
        assert i != result.intentions[id_i]
        assert i.type == result.intentions[id_i].type
