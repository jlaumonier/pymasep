import unittest

import pymasep as pm
from test.unit.mocks.game_mock import GameMock
from test.unit.mocks.controller_mock import ControllerMock


def test_init(mock_environment):
    """
    :test : pm.common.Controller()
    :condition : -
    :main_result : Controller is initialized
    """
    ctrl = pm.common.Controller(environment=mock_environment)
    assert type(ctrl) == pm.common.Controller
    assert mock_environment == ctrl.environment

def test_action_choice(mock_environment):
    """
    :test : pm.common.Controller.action_choice()
    :condition: -
    :main_return : an action is returned
    """
    observation = pm.common.State(environment=mock_environment, name='state',
                                  template=mock_environment.game.templates['DefaultStateTemplate'])
    agent = pm.common.Agent(environment=mock_environment, name='agent',
                            template=mock_environment.game.templates['DefaultAgentTemplate'])
    controller = ControllerMock(mock_environment)
    action = controller.action_choice(observation, agent)
    assert type(action) == pm.common.Action
