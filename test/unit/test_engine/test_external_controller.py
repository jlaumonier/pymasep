from omegaconf import OmegaConf

import pymasep as pm
from pymasep.communication import Message
from pymasep.application import ConnectedSubAppInfo
from test.unit.mocks.game_mock import GameMock
from test.unit.mocks.app_mock import AppMock


def test_action_choice_turn(mock_game):
    app = AppMock()
    cfg = OmegaConf.create()
    engine = pm.engine.Engine(app=app, received_q_id=0, cfg=cfg)
    engine.register_connected_sub_app(sub_app_id='id_interface', role=ConnectedSubAppInfo.ROLE_ACTOR, sent_q=None)
    env = engine.environment
    env.game = mock_game
    env.game.coord_method['play'] = GameMock.MULTIPLAYER_COORDINATION_TURN
    observation = env.create_object(name='State', template=env.game.templates['SystemStateTemplate'])
    agent = env.create_object(name='agent', template=env.game.templates['DefaultAgentTemplate'], parent=observation)
    engine.connected_sub_app['id_interface'].agent_fname = agent.get_fullname()
    controller = pm.engine.ExternalController(env)
    controller.engine = engine
    action_message = Message(Message.MESSAGE_TYPE_ACTION,
                             {'action': pm.common.Action(None, GameMock.ACTION_TYPE_1)},
                             'id_interface')
    action_message.from_bytes(action_message.to_bytes())
    engine.current_event = action_message

    action = controller.action_choice(observation, agent)

    assert isinstance(action, pm.common.Action)
    assert action.type == GameMock.ACTION_TYPE_1


def test_action_choice_waiting(mock_game):
    app = AppMock()
    cfg = OmegaConf.create()
    engine = pm.engine.Engine(app=app, received_q_id=0, cfg=cfg)
    engine.register_connected_sub_app(sub_app_id='id_interface', role=ConnectedSubAppInfo.ROLE_ACTOR, sent_q=None)
    env = engine.environment
    env.game = mock_game
    env.game.coord_method['play'] = GameMock.MULTIPLAYER_COORDINATION_WAITING_ALL
    observation = env.create_object(name='State', template=env.game.templates['SystemStateTemplate'])
    agent = env.create_object(name='agent', template=env.game.templates['DefaultAgentTemplate'], parent=observation)
    engine.connected_sub_app['id_interface'].agent_fname = agent.get_fullname()
    controller = pm.engine.ExternalController(env)
    controller.engine = engine
    action_message = Message(Message.MESSAGE_TYPE_ACTION,
                             {'action': pm.common.Action(None, GameMock.ACTION_TYPE_1)},
                             'id_interface')
    action_message.from_bytes(action_message.to_bytes())
    engine.current_event = action_message

    action = controller.action_choice(observation, agent)

    assert isinstance(action, pm.common.Action)
    assert action.type == GameMock.ACTION_TYPE_1
