from xml.etree.ElementTree import tostring, fromstring

from pymasep.circular_head_list import CircularHeadList


def test_init_empty():
    chl = CircularHeadList()

    assert len(chl) == 0
    assert chl.head is None


def test_init_with_list():
    chl = CircularHeadList([1, 2])

    assert len(chl) == 2
    assert chl.head == 1


def test_add_empty():
    chl = CircularHeadList()
    chl.append('Element1')

    assert chl[0] == 'Element1'
    assert chl.head == 'Element1'
    assert len(chl) == 1


def test_add_not_empty():
    chl = CircularHeadList(['Element1'])
    chl.append('Element2')

    assert chl.head == 'Element1'
    assert len(chl) == 2


def test_next_empty():
    chl = CircularHeadList()

    assert chl.next() is None


def test_next_one_element():
    chl = CircularHeadList(['Element1'])

    assert chl.next() == 'Element1'


def test_next_two_element_head_first():
    chl = CircularHeadList(['Element1', 'Element2'])

    assert chl.next() == 'Element2'


def test_next_two_element_head_second():
    chl = CircularHeadList(['Element1', 'Element2'])
    chl.move_head_next()

    assert chl.next() == 'Element1'


def test_move_head_next_empty():
    chl = CircularHeadList()
    chl.move_head_next()

    assert chl.head is None


def test_move_head_next_one_element():
    chl = CircularHeadList(['Element1'])
    chl.move_head_next()

    assert chl.head == 'Element1'


def test_move_head_next_two_element_head_first():
    chl = CircularHeadList(['Element1', 'Element2'])
    chl.move_head_next()

    assert chl.head == 'Element2'


def test_move_head_next_two_element_head_second():
    chl = CircularHeadList(['Element1', 'Element2'])
    chl.move_head_next()
    chl.move_head_next()

    assert chl.head == 'Element1'


def test_insert_empty_at_first():
    chl = CircularHeadList()
    chl.insert(0, 'Element1')

    assert chl.data == ['Element1']
    assert chl.head == 'Element1'
    assert len(chl) == 1


def test_insert_empty_at_second():
    chl = CircularHeadList()
    chl.insert(1, 'Element1')

    assert chl.data == ['Element1']
    assert chl.head == 'Element1'
    assert len(chl) == 1


def test_insert_one_element_at_first():
    chl = CircularHeadList(['Element2'])
    chl.insert(0, 'Element1')

    assert chl.head == 'Element2'
    assert len(chl) == 2


def test_insert_one_element_at_head():
    chl = CircularHeadList(['Element2'])
    chl.insert(1, 'Element1')

    assert chl.head == 'Element2'
    assert len(chl) == 2


def test_insert_one_element_after_head():
    chl = CircularHeadList(['Element2', 'Element3'])
    chl.insert(2, 'Element1')

    assert chl.head == 'Element2'
    assert len(chl) == 3


def test_to_xml_empty():
    expected_xml_str = '<CircularHeadList>' \
                       '<data><list /></data>' \
                       '<head_idx>-1</head_idx></CircularHeadList>'
    chl = CircularHeadList()

    xml_str_result = tostring(chl.to_xml(), encoding='unicode')

    assert xml_str_result == expected_xml_str


def test_to_xml_not_empty():
    expected_xml_str = '<CircularHeadList>' \
                       '<data>' \
                       '<list><item><str>Element1</str></item><item><str>Element2</str></item></list>' \
                       '</data>' \
                       '<head_idx>1</head_idx></CircularHeadList>'
    chl = CircularHeadList(['Element1', 'Element2'])
    chl.move_head_next()

    xml_str_result = tostring(chl.to_xml(), encoding='unicode')

    assert xml_str_result == expected_xml_str


def test_from_xml_empty():
    chl_xml_str = '<CircularHeadList>' \
                  '<data><list /></data>' \
                  '<head_idx>-1</head_idx></CircularHeadList>'
    xml_node = fromstring(chl_xml_str)
    chl = CircularHeadList()
    chl.from_xml(xml_node=xml_node)

    assert chl.head is None
    assert len(chl) == 0


def test_from_xml_not_empty():
    chl_xml_str = '<CircularHeadList>' \
                  '<data><list><item><str>Element1</str></item><item><str>Element2</str></item></list></data>' \
                  '<head_idx>1</head_idx></CircularHeadList>'
    xml_node = fromstring(chl_xml_str)
    chl = CircularHeadList()
    chl.from_xml(xml_node=xml_node)

    assert len(chl) == 2
    assert chl.head == 'Element2'
