#!/bin/bash
# $1 version/branch
BRANCH=$1
git clone git@gitlab.com:jlaumonier/pymasep.git --branch $BRANCH

ROOT_PATH=`pwd`

LIST_PROJECTS="encartgames victorsworld pymasep-examples"

download() {
    PROJECT=$1
    git clone git@gitlab.com:jlaumonier/$PROJECT.git
    cd $PROJECT

    PROJ_BRANCH=dev
    if  git show-ref -q $BRANCH; then
        echo "Git branch '$BRANCH' exists in the remote repository"
        PROJ_BRANCH=$BRANCH
    fi
    git checkout $PROJ_BRANCH
    cd $ROOT_PATH
}
fis
package() {
    PROJECT=$1
    cd $PROJECT
    python3 -m venv venv
    source venv/bin/activate
    pip install -r requirements.txt
    pip uninstall -y pymasep
    pip install ../pymasep
    cd packaging
    ./installer.sh
    cd dist
    for d in * ; do
        if [ -d "$d" ]; then
            zip -r "$d-$BRANCH.zip" $d
            echo $d
            cp $d-$BRANCH.zip $ROOT_PATH
        fi
    done
    cd $ROOT_PATH
}

for p in $LIST_PROJECTS ; do
    download $p
done

for p in $LIST_PROJECTS ; do
    package $p
done

