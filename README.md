# Pymasep v0.2

Python MultiAgent System Engine Plateform.

This library is used to create (simple) games or Markov games for reinforcement learning. 

Pymasep a kind of game engine with different concepts from traditional game engine. Mainly, this tool is not based on representing the world from video/display perspectives but with object concepts that can be displayed later with different approaches. It is mainly a tool for proof of concepts and improving my development skills.

See https://gitlab.com/jlaumonier/pymas-examples and https://gitlab.com/jlaumonier/encartgames for examples of this library.

see https://pymasep.readthedocs.io for complete documentation

Do not hesitate to comment/open issues to improve this tool to be more useful for other use cases.