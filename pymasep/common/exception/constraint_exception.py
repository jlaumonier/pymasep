class ConstraintException(Exception):
    """
    Exception that can occur during the run of the game when a constraint in a pymasep object is violated
    """
    pass
