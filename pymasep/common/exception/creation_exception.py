class CreationException(Exception):
    """
    Exception that can occur during the creation of a pymasep object
    """
    pass
